# 1

# On The Criteria To Be Used in Decomposing Systems into Modules  

Title: On The Criteria To Be Used in Decomposing Systems into Modules
Keywords: Decomposing, Modularization, Changeability, Heirarchal, Modules, Software, KWIC index, software design

**First Paragraph:**

The paper, "On The Criteria To Be Used in Decomposing Systems into Modules", discusses modularization as a mechanism for improving the flexibility and comprehensibility of a system while allowing the shortening of its development time. The paper aims to discuss the issue of dividing systems into modules and suggests the criteria which can be used in decomposing a system into modules. The article demonstrates two examples of Modularization and concludes by stating that almost always incorrect to begin the decomposition of a system into modules on the basis of a flowchart. Instead of doing this, the article proposes that one begins with a list of difficult design decisions or design decisions which are likely to change. 


**Second Paragraph:**

Overall I did not enjoy the layout of this paper and descriptions of modularization. I thought parts were confusing and it was hard to read. I think the strength of the paper lie in differentiating between modularization 1 & 2. The weaknesses were the layout, and getting across complex topics in simplistic terms. 

**My Design Approaches**

My design approaches were based off of how we were taught to tackle the program in class. I did not segregate each action like the module notes below, but I did have a segments of code for reading input, walking characters, alphabetizing, etc. Overall, it was hard to comprehend some parts of this paper as it seems they split the problem up into too many subproblems. 
 
**notes**

- Major advancements in modular programming come from The development of coding techniques and assemblers which
- 1, allow one module to be written with little knowledge of the code in another module and 
- 2, allow modules to be reassembled and replaced without reassembly of the whole system 

- The benefits of modular programming are:
- 1 managerial - dev time should be less 
- 2 product felxibility - it should be possible to make durastic changes to one module without a need to change others
- 3 comphrensibility - it should be possible to study the system one module at a time 
- the whole system can therefore be better designed because it is better understood 

- What is modularization? 
- module is considered to be a responsibility assignment rather than a sub program 
- modularizations include the design decisions which must be made before the work on independent modules can begin
- system level == affect more than one module 

- KWIC index system accepts an ordered set of lines, each line is an ordered set of words, and each word is an ordered set of characters
- Any line may be "circularly shifted" by repeatedly removing the first word and appending it at the end of the line. The KWXC index system outputs a listing of all circular shifts of all lines in alphabetical order. 

- Modularization 1

- Modularization 1 Input: 
- INPUT - this program reads input medium and stores them in core for processing by the remaining modules 
- Characters are packed four to a word and an otherwise unused character is used to indicate the end of a word
- An index is kept to show the starting address of each line. 

- Modularization 2 Circular Shift: 
- This module is called after the input module has completed its work
- prepares an index which gives the address of the first character of each circular shift, and the original index of the line in the array made up by module 1
- leaves its output in core with words in pairs (original line number, starting address). 

- Modularization 3 Alphabetizing: 
- This module takes an input the arrays produced by modules 1 and 2.
- It produces an array in the same format as that produced by module 2. In this case, however, the circular shifts are listed in another order (alphabetically). 

- Modularization 5 Master Control: 
- This module does little more than control the sequencing among the other four modules
- It may also handle rror messages, space allocation, etc

- Modularization 2 

- Module 1: line storage 
- This module consists of a number of functions or subroutines which provide the means by which the user of the module may call on it. 

- Module 2: input
- This module reads the original lines from the input media and calls the line storage module to have them stored internally

- Module 3: circular shifter 
- The principal functions provided by this module are analogs of functions provided in module 1

- Module 4: Alphabetizer 
- consists of ALPH and ITH 

- Module 5: Output
- This module will give the desired printing of set of lines or circular shifts. 

- Module 6: Master of Control 
-  Similar in function to the modularization above. 

- Comparison 
- Both of these schemes will work 
- Both will reduce the programming to the relatively independent programming of a number of small, manageable, programs. 
- If we are not careful the second decomposition will prove to be much less efficient than the first. 


# On the criteria to be used in decomposing systems into modules (summary)

Title: On The Criteria To Be Used in Decomposing Systems into Modules
Keywords: Decomposing, Modularization, Changeability, Heirarchal, Modules, Software, KWIC index, software design

**First Paragraph**

The summary, On the criteria to be used in decomposing systems into modules, is a summary of the 45 year old paper form above. It goes on to discuss how "it’s not simply about having lots of small modules, a large part of the success or otherwise of your system depends on how you choose to divide the system into modules in the first place". Additionally, the article talks about the three succint benefits of modular programming: one is development time should be shortened, two is product flexibility should be improved, and three is comprehensibility. The article iterates on a lot of the topics discussed above in an easier to read fashion. 

**Second Paragraph** 

This article was a lot stronger than the one above it, there are nice depictions and it's way easier to understand. There really are no weaknesses, other than that I wish I had read this before the more complex explanation. 

# Charting example

![chart](/Users/mrcisme/Desktop/demojfreechart/assets/Chart.png)

**Design Write-Up**

In order to implement updating the chart when the data files are updated, using the Observer pattern, 
a few ideas come to mind. First, it is important to figure out what our goal is. We want to display the provided stock data in the CSV folder,
plot the price data from the CSV on a time series chart, and update the chart when the data files change.
We were provided a repository of a jfreechart example and when we run the code, and we get the output above.

To display the CSV data, we are going to have to read in the data from the file.
We can do this various ways, but I'm assuming we can use BufferedReader in = new BufferedReader(new FileReader("APPL.txt"));

We would use the Observer pattern to add an action listener. We need an action listener that points from the chart
to the Stock Data class where the listeners are defined. In the Stock Data class, we could create a read data method, 
and add/remove listener. The reason for the listener is to notify for when an object (chart object) changes state. 
Lastly, we will use the TimeSeriesDemo1 class, the stock data class, and the event listener method. 


# 2 

<https://gitlab.oit.duke.edu/mrc90/assignment-5-refactoring-chapter-1>
<https://gitlab.oit.duke.edu/mrc90/demojfreechart>

# 3 

#Reflection 

Overall, everything went well. I tried to write code for the charting example, but I had some trouble parsing.
I will continue to try and get help in class. 

Refactoring was done with Jonathan Rhodes (Net ID: JDR85)


# Times for tasks and additional notes

* On The Criteria To Be Used in Decomposing Systems into Modules + Notes - 2 hours
* On the criteria to be used in decomposing systems into modules (summary) + Notes - 30 mins
* Refactoring example- 4 hours
* Charting example - 2 hours 



